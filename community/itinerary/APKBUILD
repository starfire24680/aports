# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=itinerary
pkgver=23.08.0
pkgrel=0
# armhf blocked by qt5-qtdeclarative
# ppc64le blocked by kitinerary
# s390x blocked by qt5-qtdeclarative
arch="aarch64 armv7 x86_64 x86"
url="https://invent.kde.org/pim/itinerary"
pkgdesc="Itinerary and boarding pass management application"
license="BSD-3-Clause AND LGPL-2.0-or-later"
depends="
	kirigami-addons
	kirigami2
	kitemmodels
	kopeninghours
	prison
	qt5-qtlocation
	tzdata
	"
makedepends="
	extra-cmake-modules
	kcontacts-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kholidays-dev
	ki18n-dev
	kirigami-addons-dev
	kitinerary-dev
	knotifications-dev
	kosmindoormap-dev
	kpkpass-dev
	kpublictransport-dev
	networkmanager-qt-dev
	qqc2-desktop-style-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtlocation-dev
	qt5-qtquickcontrols2-dev
	samurai
	shared-mime-info
	solid-dev
	zlib-dev
	"
checkdepends="
	dbus
	xvfb-run
	"
_repo_url="https://invent.kde.org/pim/itinerary.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/itinerary-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	# tripgrouptest and timelinemodel test are broken
	dbus-run-session xvfb-run ctest --test-dir build --output-on-failure -E "(tripgroup|timelinemodel)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
9e149ef2b31ee9a745834afc94b8ee2016fdbaf5022ab51a431ae7c0c77fd9a1deb557c23dff2823e81322c96b6d8fb44bb109aad4d69f74b4317fa9445c97f8  itinerary-23.08.0.tar.xz
"
