# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=py3-podman
pkgver=4.6.0
pkgrel=0
pkgdesc="Python bindings for Podman's RESTful API"
url="https://github.com/containers/podman-py"
license="Apache-2.0"
arch="noarch"
depends="py3-requests py3-urllib3 py3-xdg"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
checkdepends="py3-pytest py3-requests-mock py3-fixtures"
subpackages="$pkgname-pyc"
source="https://github.com/containers/podman-py/archive/v$pkgver/py3-podman-$pkgver.tar.gz"
builddir="$srcdir/podman-py-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	pytest podman/tests/unit
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/podman-$pkgver-py3-none-any.whl
}

sha512sums="
4d60d3643e679d68f560988c2b31272703059dc000e6361b44d896df480a72f36df595f0122ee39f44874b76a7286bbf05ea8c045e5afa58bd0c7b0a5a42f2de  py3-podman-4.6.0.tar.gz
"
