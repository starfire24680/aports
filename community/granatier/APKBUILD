# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=granatier
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/games/org.kde.granatier"
pkgdesc="A clone of the classic Bomberman game"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	knewstuff-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libkdegames-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	samurai
	"
_repo_url="https://invent.kde.org/games/granatier.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/granatier-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
dc96ca7adb1df7fc5a463f3beb9f8a0a8e13a6bf80d5a8516db2fe30bafe7de7d787428f6973e91c3db52e318f35a1201a581ca6f70479bfe5e6776b811c8b13  granatier-23.08.0.tar.xz
"
