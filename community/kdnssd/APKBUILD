# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kdnssd
pkgver=5.109.0
pkgrel=0
arch="all !armhf" # armhf blocked by extra-cmake-modules
pkgdesc="Network service discovery using Zeroconf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-or-later"
depends="avahi"
depends_dev="
	avahi-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
_repo_url="https://invent.kde.org/frameworks/kdnssd.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kdnssd-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/kdnssd.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
c025e1ce195267b6c3743b54ddf01f3cd141b4e3e71f34cd61435e8a9ca2bdacf6baa502772e5cd064bead856f2e6c86fe90a47314a9d6dd912ee9d95ce83baa  kdnssd-5.109.0.tar.xz
"
