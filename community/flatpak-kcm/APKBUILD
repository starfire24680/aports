# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=flatpak-kcm
pkgver=5.27.7
pkgrel=1
pkgdesc="Flatpak Permissions Management KCM"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
depends="kirigami2"
makedepends="
	extra-cmake-modules
	flatpak-dev
	kcmutils-dev
	kconfig-dev
	kdeclarative-dev
	ki18n-dev
	kitemmodels-dev
	qt5-qtdeclarative-dev
	qt5-qtsvg-dev
	samurai
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
_repo_url="https://invent.kde.org/plasma/flatpak-kcm.git"
source="https://download.kde.org/$_rel/plasma/$pkgver/flatpak-kcm-$pkgver.tar.xz"
subpackages="$pkgname-lang"
install_if="flatpak systemsettings"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/plasma/flatpak-kcm.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build

	# flatpakpermissiontest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "flatpakpermissiontest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e8261988e4ea215ef4d0e7544779f64e33d2c3b92573719c12d3ce49d4ca956bf3bee43d6a371f58cdf5303abf3944baf88888fddbed14c9a63f574caeff41b3  flatpak-kcm-5.27.7.tar.xz
"
