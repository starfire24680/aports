# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=py3-pypdf
pkgver=3.15.0
pkgrel=0
pkgdesc="Pure-Python library built as a PDF toolkit"
url="https://github.com/py-pdf/pypdf"
arch="noarch"
license="BSD-3-Clause"
options="!check" # issues with reading pdf files from test dirs
makedepends="py3-gpep517 py3-installer py3-flit-core"
checkdepends="py3-pillow py3-pytest"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/py-pdf/pypdf/archive/refs/tags/$pkgver.tar.gz"
builddir="$srcdir/pypdf-$pkgver"

provides="py3-pypdf2=$pkgver-r$pkgrel"
replaces="py3-pypdf2"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
a9dc59141918fe47d1b2ec0fc282e31c05ef3df137cc67e85d965172929b3b1bbc38f8827a82a576f02c18b72cdca297364f58814f960b7783a36228a7e29125  py3-pypdf-3.15.0.tar.gz
"
