# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=py3-trove-classifiers
pkgver=2023.8.7
pkgrel=0
pkgdesc="Canonical source for classifiers on PyPI"
url="https://github.com/pypa/trove-classifiers"
license="Apache-2.0"
arch="noarch"
depends="python3"
makedepends="py3-calver py3-gpep517 py3-installer py3-setuptools py3-wheel"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="https://github.com/pypa/trove-classifiers/archive/$pkgver/py3-trove-classifiers-$pkgver.tar.gz"
builddir="$srcdir/trove-classifiers-$pkgver"

prepare() {
	default_prepare

	echo "Version: $pkgver" > PKG-INFO
}

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/trove_classifiers-$pkgver-py3-none-any.whl
}

sha512sums="
4a07811ac9fae5afbf5b105a05d399f7ef94c462194353e17cc1ec3725f15b8eb574c6281b9f2b656f9838761640df92b9d5d79216f4ecd194bd96509d6e1712  py3-trove-classifiers-2023.8.7.tar.gz
"
