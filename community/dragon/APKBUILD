# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=dragon
pkgver=23.08.0
pkgrel=0
pkgdesc="A multimedia player where the focus is on simplicity, instead of features"
url="https://kde.org/applications/multimedia/org.kde.dragonplayer"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
license="GPL-2.0-only OR GPL-3.0-only"
depends="phonon-backend-gstreamer"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kjobwidgets-dev
	knotifications-dev
	kparts-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	phonon-dev
	qt5-qtbase-dev
	samurai
	solid-dev
	"
_repo_url="https://invent.kde.org/multimedia/dragon.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/dragon-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
4e6541706030b6f8c806e1ef709fcb3b523b29d3cef45402bdc2249a85d12167f0694dee22a9fc4ca0df7449d66e82fa0dc684726d7aa74b21c928d2f2c66309  dragon-23.08.0.tar.xz
"
