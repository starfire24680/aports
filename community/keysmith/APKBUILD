# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=keysmith
pkgver=23.08.0
pkgrel=0
pkgdesc="OTP client for Plasma Mobile and Desktop"
url="https://invent.kde.org/kde/keysmith"
arch="all !armhf"
license="GPL-3.0-or-later"
depends="kirigami2"
makedepends="
	extra-cmake-modules
	kdbusaddons-dev
	ki18n-dev
	kirigami2-dev
	libsodium-dev
	qt5-qtbase-dev
	qt5-qtquickcontrols2-dev
	qt5-qtsvg-dev
	samurai
	"
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/utilities/keysmith.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/keysmith-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
75b43fdeef928d2e9ee584f07e340e9cdd441ceae689b2d7226e2603730cef768823eabdbf29b678a4f1f446e1e493bfed3cb5fc1d7864267aadfeb868470d03  keysmith-23.08.0.tar.xz
"
