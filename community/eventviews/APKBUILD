# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=eventviews
pkgver=23.08.0
pkgrel=0
pkgdesc="Library for creating events"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> akonadi
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org/"
license="GPL-2.0-or-later"
depends_dev="
	akonadi-calendar-dev
	akonadi-dev
	calendarsupport-dev
	kcalendarcore-dev
	kcalutils-dev
	kcodecs-dev
	kcompletion-dev
	kdiagram-dev
	kguiaddons-dev
	kholidays-dev
	ki18n-dev
	kiconthemes-dev
	kmime-dev
	kservice-dev
	libkdepim-dev
	qt5-qtbase-dev
	"
checkdepends="xvfb-run"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	samurai
	"
_repo_url="https://invent.kde.org/pim/eventviews.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/eventviews-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
c2ed3f6dd499a038f47d9939b9ece5ab9bd34d45f25ef6a8007a02eba5c89089616aa7dd17750704c22cd727a2880f11b7acb6b5051d2dd61815694c537747e6  eventviews-23.08.0.tar.xz
"
