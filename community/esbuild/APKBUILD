# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=esbuild
pkgver=0.19.2
pkgrel=0
pkgdesc="Extremely fast JavaScript bundler and minifier"
url="https://esbuild.github.io/"
license="MIT"
arch="all"
makedepends="go nodejs"
source="https://github.com/evanw/esbuild/archive/v$pkgver/esbuild-$pkgver.tar.gz"
options="net" # fetch dependencies

export GOPATH="$srcdir"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build \
		-ldflags="-X main.version=$pkgver" \
		-v ./cmd/esbuild

	node scripts/esbuild.js npm/esbuild/package.json --version
	node scripts/esbuild.js ./esbuild --neutral

	# binary path override
	sed -i '1s#^#var ESBUILD_BINARY_PATH = "/usr/bin/esbuild";\n#' \
		npm/esbuild/lib/main.js
}

check() {
	go test ./...
}

package() {
	install -Dm755 esbuild "$pkgdir"/usr/bin/esbuild

	local destdir=/usr/lib/node_modules/esbuild

	install -d \
		"$pkgdir"/$destdir/bin \
		"$pkgdir"/$destdir/lib

	install -Dm644 -t "$pkgdir"/$destdir npm/esbuild/package.json
	install -Dm644 -t "$pkgdir"/$destdir/lib npm/esbuild/lib/*
	ln -s /usr/bin/esbuild "$pkgdir"/$destdir/bin/esbuild
}

sha512sums="
5ec21e4992610742f04fa7ce920208fa0d24c101cc973e616246caaa64fa1d9aeb73ee4d3b8ca4ea36555c0a8469208f0addd89645f1537f59f685aea3dae710  esbuild-0.19.2.tar.gz
"
