# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kmix
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/multimedia/org.kde.kmix"
pkgdesc="A sound channel mixer and volume control"
license="GPL-2.0-or-later AND LGPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	alsa-lib-dev
	extra-cmake-modules
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kglobalaccel-dev
	ki18n-dev
	kiconthemes-dev
	knotifications-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	libcanberra-dev
	plasma-framework-dev
	pulseaudio-dev
	qt5-qtbase-dev
	samurai
	solid-dev
	"
_repo_url="https://invent.kde.org/multimedia/kmix.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kmix-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
54aa72fe4a966976b61c0ad5332bdcd5376a83d4f5c8d317fba6c27720c7a057cff650ef58734e6840a4f0ee99eb1b91fbe44e4ac0198e70b7bec600594cdda0  kmix-23.08.0.tar.xz
"
