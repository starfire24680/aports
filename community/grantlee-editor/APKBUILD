# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=grantlee-editor
pkgver=23.08.0
pkgrel=0
pkgdesc="Utilities and tools to manage themes in KDE PIM applications "
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org/"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	akonadi-mime-dev
	extra-cmake-modules
	grantleetheme-dev
	karchive-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kimap-dev
	knewstuff-dev
	kpimtextedit-dev
	ktexteditor-dev
	kxmlgui-dev
	libkleo-dev
	messagelib-dev
	pimcommon-dev
	qgpgme
	qt5-qtbase-dev
	qt5-qtwebengine-dev
	samurai
	syntax-highlighting-dev
	"
_repo_url="https://invent.kde.org/pim/grantlee-editor.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/grantlee-editor-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
49d61d2a8fc77a0bf8183fdf73c252e5ca233e14a2792bef711825fa253fae8302e9ede0187fd4a0b42db42185caae1226f0c95f73d2eb57601dc9da30129cc9  grantlee-editor-23.08.0.tar.xz
"
