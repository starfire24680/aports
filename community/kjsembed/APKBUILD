# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kjsembed
pkgver=5.109.0
pkgrel=0
pkgdesc="JavaScript bindings for QObject"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="
	ki18n-dev
	kjs-dev
	qt5-qtsvg-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	kdoctools-dev
	qt5-qttools-dev
	samurai
	"
_repo_url="https://invent.kde.org/frameworks/kjsembed.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/kjsembed-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/kjsembed.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
705d11934e767b6ac9c5d59a9e19f07cb6cef31757d7111033040cbef3022f12ce3ddbf4a12a6aeffebcf3526b04872dd2c114c77667f10d671e6d4e23412e04  kjsembed-5.109.0.tar.xz
"
