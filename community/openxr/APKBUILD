# Contributor: Leon Marz <main@lmarz.org>
# Maintainer: Leon Marz <main@lmarz.org>
pkgname=openxr
pkgver=1.0.29
pkgrel=0
pkgdesc="OpenXR loader library"
url="https://khronos.org/openxr"
arch="all"
license="Apache-2.0"
makedepends="cmake mesa-dev vulkan-loader-dev jsoncpp-dev samurai wayland-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/KhronosGroup/OpenXR-SDK/archive/release-$pkgver.tar.gz"
builddir="$srcdir/OpenXR-SDK-release-$pkgver"
options="!check" # no check available

build() {
	case "$CARCH" in
		armhf)
			export CXXFLAGS="$CXXFLAGS -D_M_ARM" ;;
	esac

	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_BUILD_TYPE=Release
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
ccb277aa9297ccc8574b2aec59d9205630e4b97302958eb70dcfc165789704154bbf49fd3647a75b2ee8673791655e08c67035c7e196bbc9711ff791aa859811  openxr-1.0.29.tar.gz
"
