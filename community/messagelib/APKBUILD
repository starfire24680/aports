# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=messagelib
pkgver=23.08.0
pkgrel=0
pkgdesc="KDE PIM messaging library"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org/"
license="LGPL-2.0-only OR LGPL-3.0-only AND GPL-2.0-or-later"
depends_dev="
	akonadi-contacts-dev
	akonadi-dev
	akonadi-mime-dev
	akonadi-search-dev
	gpgme-dev
	grantlee-dev
	grantleetheme-dev
	karchive-dev
	kcodecs-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcontacts-dev
	kdbusaddons-dev
	ki18n-dev
	kiconthemes-dev
	kidentitymanagement-dev
	kio-dev
	kitemviews-dev
	kjobwidgets-dev
	kldap-dev
	kmailtransport-dev
	kmbox-dev
	kmime-dev
	knewstuff-dev
	kpimtextedit-dev
	kservice-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	libgravatar-dev
	libkdepim-dev
	libkleo-dev
	pimcommon-dev
	qca-dev
	qt5-qtbase-dev
	qt5-qtwebengine-dev
	sonnet-dev
	syntax-highlighting-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/pim/messagelib.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/messagelib-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"
options="!check" # Requires OpenGL and running dbus server

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
ccbe353edecb79474e808739df1b3637e9eb631787a923156c455feaa1a096948534cd8efb8c30caf8f7c8cdc7a823556c6f29869b2e4a6f65d648772a2818b9  messagelib-23.08.0.tar.xz
"
