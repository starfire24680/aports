# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=libkleo
pkgver=23.08.0
pkgrel=0
pkgdesc="KDE PIM cryptographic library"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kontact.kde.org"
license="GPL-2.0-or-later"
# TODO: Maybe replace gnupg with specific gnupg subpackages.
depends="gnupg"
makedepends="
	boost-dev
	extra-cmake-modules
	gpgme-dev
	kcodecs-dev
	kcompletion-dev
	kconfig-dev
	kcoreaddons-dev
	ki18n-dev
	kitemmodels-dev
	kpimtextedit-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	qt5-qtbase-dev
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/pim/libkleo.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkleo-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure -E "newkeyapprovaldialogtest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
cb76c668231f0563c27c8df600703c800c70c297c0e1058da9320babea9827a3faf1239be94aac96f3c244287a0524e8e231c045e96d6ba68f30f23047ba5d55  libkleo-23.08.0.tar.xz
"
