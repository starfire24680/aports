# Contributor: Henrik Riomar <henrik.riomar@gmail.com>
# Maintainer: Henrik Riomar <henrik.riomar@gmail.com>
pkgname=netdata-go-plugins
pkgver=0.54.1
pkgrel=0
pkgdesc="netdata go.d.plugin"
url="https://github.com/netdata/go.d.plugin"
arch="all !x86 !armv7 !armhf" # checks fail
license="GPL-3.0-or-later"
depends="netdata"
makedepends="go"
source="$pkgname-$pkgver.tar.gz::https://codeload.github.com/netdata/go.d.plugin/tar.gz/refs/tags/v$pkgver"
builddir="$srcdir/go.d.plugin-$pkgver"

export GOFLAGS="$GOFLAGS -modcacherw"
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -v -o go.d.plugin ./cmd/godplugin
}

check() {
	go test ./...
}

package() {
	 mkdir -p "$pkgdir/usr/lib/netdata/conf.d"
	 cp -r "$builddir/config/go.d.conf" "$builddir/config/go.d" "$pkgdir/usr/lib/netdata/conf.d/"

	 mkdir -p "$pkgdir/usr/libexec/netdata/plugins.d/"
	 install -D -m755 -t "$pkgdir/usr/libexec/netdata/plugins.d" "$builddir/go.d.plugin"
}

sha512sums="
a3d7148b358ee5b239b933bc28b43065e21f5f4d0e5297198616ca2fcf5a950d606810d4758f0751c486541508236a2ae3495feb7f53c4813671cee312257ca7  netdata-go-plugins-0.54.1.tar.gz
"
