# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kaccounts-providers
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x, ppc64le and riscv64 blocked by qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kde.org/applications/internet/"
pkgdesc="Small system to administer web accounts for the sites and services across the KDE desktop"
license="GPL-2.0-or-later"
depends="
	signon-plugin-oauth2
	"
makedepends="
	extra-cmake-modules
	kaccounts-integration-dev
	kdeclarative-dev
	ki18n-dev
	kio-dev
	kpackage-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtwebengine-dev
	samurai
	"
_repo_url="https://invent.kde.org/network/kaccounts-providers.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kaccounts-providers-$pkgver.tar.xz"
options="!check" # No tests
subpackages="$pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
b2f2181b67eaeefd4f265c8bee3484d3ae5da8d2e595bf1957e7547da9a5a471381226037435433af3efbf627b893124063a2d4fc7ca6f625e72e13583106a53  kaccounts-providers-23.08.0.tar.xz
"
