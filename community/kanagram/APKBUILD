# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kanagram
pkgver=23.08.0
pkgrel=0
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://edu.kde.org/kanagram/"
pkgdesc="Letter Order Game"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdeclarative-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	knewstuff-dev
	libkeduvocdocument-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	samurai
	sonnet-dev
	"
_repo_url="https://invent.kde.org/education/kanagram.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kanagram-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
f2be77fd4cdafeee58f6d83ca091c332b5d26454b447de8d2ac7787cec10ecd6a4ed059b3a0047ac7e5840bde64be4f2b12ee0d2c92716d734a015f2127da264  kanagram-23.08.0.tar.xz
"
