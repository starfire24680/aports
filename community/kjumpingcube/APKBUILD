# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kjumpingcube
pkgver=23.08.0
pkgrel=0
pkgdesc="A simple dice driven tactical game"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://apps.kde.org/kjumpingcube/"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libkdegames-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	samurai
	"
_repo_url="https://invent.kde.org/games/kjumpingcube.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kjumpingcube-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
9a5ec201b2a7b4454bdd771e29baee45fb5567c3291ce5724ee20cc1604d323c21c2cffaf2f9d2d70161c80834150bf3444101265de00ccf458ebd0c26ea066d  kjumpingcube-23.08.0.tar.xz
"
