# Contributor: <xmingske@gmail.com>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=gsoap
pkgver=2.8.130
pkgrel=0
arch="all"
pkgdesc="Cross-platform C and C++ SDK for SOAP/XML Web services"
license="GPL-2.0-or-later gSOAP-1.3b"
url="https://www.genivia.com/dev.html"
depends_dev="$pkgname-tools=$pkgver-r$pkgrel"
makedepends="autoconf automake bison flex openssl-dev>3 libtool zlib-dev"
subpackages="$pkgname-tools $pkgname-dev"
source="https://prdownloads.sourceforge.net/gsoap2/gsoap_$pkgver.zip
	gsoap-libtool.patch
	musl-fixes.patch
	"
builddir="$srcdir/$pkgname-${pkgver%.*}"

# secfixes:
#   2.8.113-r0:
#     - CVE-2021-21783

prepare() {
	default_prepare
	autoreconf -vif
}

build() {
	export CFLAGS="$CFLAGS -D_GNU_SOURCE"
	./configure \
		--prefix=/usr \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--exec-prefix=/usr \
		--enable-ipv6 \
		--enable-c-locale \
		--disable-static
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

tools() {
	pkgdesc="$pkgdesc (tools)"

	amove usr/bin
}

sha512sums="
b82ebc7d058a9e2f59d325e2c50e96f6bd712c65285389cb120c4a9bbf51da1b99a833d8f81ba674e37f34090fde8317990de1906c97434342cbe84568d66b6c  gsoap_2.8.130.zip
079bb9191f7c756d8e3d87a48f412ff60ef1190b6a1353cdd7ef54b6ade270152bd628b102332a2fdf7b8bbeeb9c7c387c6741c23d10d4024e691c3b260a9ef4  gsoap-libtool.patch
2e02b2149a8c16236d372dfac3613b94c047d09e0095bc45d426d0b2fbcededd456955042fd1462b6ffb621228cfa72cf7ae44114c9495fe3ae900b0f9a6ebf0  musl-fixes.patch
"
