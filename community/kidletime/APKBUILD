# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kidletime
pkgver=5.109.0
pkgrel=0
pkgdesc="Monitoring user activity"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-only"
makedepends="
	doxygen
	extra-cmake-modules
	plasma-wayland-protocols
	qt5-qttools-dev
	qt5-qtwayland-dev
	qt5-qtx11extras-dev
	samurai
	wayland-dev
	wayland-protocols
	"
_repo_url="https://invent.kde.org/frameworks/kidletime.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kidletime-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/kidletime.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	# solidmttest is broken
	ctest --test-dir build --output-on-failure -E "solidmttest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
f0191415fd5e86adc60ee72b454efb03c4bb9729d0585baf8dbd2411663a58415b338ceb4a2681e90fc14df938f8c9750d6ca6dcfce4816762843bd32135997b  kidletime-5.109.0.tar.xz
"
