# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=containers-common
pkgver=0.55.3
pkgrel=0
pkgdesc="Configuration files for container tools"
url="https://github.com/containers/common"
license="Apache-2.0"
arch="noarch"
options="!check" # no test suite
makedepends="go-md2man"
subpackages="$pkgname-doc"
# Pick the exact versions of common/storage/image vendored in podman.
# Ideally, they should be the same in skopeo and buildah.
# Check them with the list_vendors function.
_common_ver=$pkgver
_storage_ver=1.48.0
_image_ver=5.26.1
_podman_ver=4.6.1
_skopeo_ver=1.13.2
_buildah_ver=1.31.2
_shortnames_ver=2023.02.20
source="https://github.com/containers/common/archive/v$_common_ver/common-$_common_ver.tar.gz
	https://github.com/containers/storage/archive/v$_storage_ver/storage-$_storage_ver.tar.gz
	https://github.com/containers/image/archive/v$_image_ver/image-$_image_ver.tar.gz
	https://github.com/containers/podman/archive/v$_podman_ver/podman-$_podman_ver.tar.gz
	https://github.com/containers/skopeo/archive/v$_skopeo_ver/skopeo-$_skopeo_ver.tar.gz
	https://github.com/containers/buildah/archive/v$_buildah_ver/buildah-$_buildah_ver.tar.gz
	https://github.com/containers/shortnames/archive/v$_shortnames_ver/shortnames-$_shortnames_ver.tar.gz
	"

list_vendors() {
	unpack

	for tool in podman-$_podman_ver skopeo-$_skopeo_ver buildah-$_buildah_ver; do
		cd "$srcdir"/$tool
		msg $tool
		grep github.com/containers/common go.mod
		grep github.com/containers/storage go.mod
		grep github.com/containers/image go.mod
	done
}

prepare() {
	default_prepare

	# fix go-md2man path in containers/storage
	sed -E 's/(GOMD2MAN =).*/\1 go-md2man/' -i "$srcdir"/storage-$_storage_ver/docs/Makefile

	# set default storage driver
	sed -E 's/(driver =) ""/\1 "overlay"/' -i "$srcdir"/storage-$_storage_ver/storage.conf

	# set unqualified-search-registries
	sed -E 's/# (unqualified-search-registries =).*/\1 ["docker.io"]/' -i "$srcdir"/image-$_image_ver/registries.conf
}

build() {
	cd "$srcdir"/common-$_common_ver
	make -C docs

	cd "$srcdir"/storage-$_storage_ver
	make -C docs

	cd "$srcdir"/image-$_image_ver
	make docs
}

package() {
	install -d "$pkgdir"/etc/containers/certs.d
	install -d "$pkgdir"/etc/containers/oci/hooks.d
	install -d "$pkgdir"/var/lib/containers/sigstore

	cd "$srcdir"/common-$_common_ver
	install -Dm644 pkg/config/containers.conf "$pkgdir"/etc/containers/containers.conf
	install -Dm644 pkg/config/containers.conf "$pkgdir"/usr/share/containers/containers.conf
	install -Dm644 pkg/seccomp/seccomp.json "$pkgdir"/etc/containers/seccomp.json
	install -Dm644 pkg/seccomp/seccomp.json "$pkgdir"/usr/share/containers/seccomp.json
	make -C docs install PREFIX=/usr DESTDIR="$pkgdir"

	cd "$srcdir"/storage-$_storage_ver
	install -Dm644 storage.conf "$pkgdir"/etc/containers/storage.conf
	install -Dm644 storage.conf "$pkgdir"/usr/share/containers/storage.conf
	make -C docs install DESTDIR="$pkgdir"

	cd "$srcdir"/image-$_image_ver
	install -Dm644 registries.conf "$pkgdir"/etc/containers/registries.conf
	make install DESTDIR="$pkgdir"

	cd "$srcdir"/skopeo-$_skopeo_ver
	install -Dm644 default-policy.json "$pkgdir"/etc/containers/policy.json
	install -Dm644 default.yaml "$pkgdir"/etc/containers/registries.d/default.yaml

	cd "$srcdir"/shortnames-$_shortnames_ver
	install -Dm644 shortnames.conf "$pkgdir"/etc/containers/registries.conf.d/00-shortnames.conf
}

doc() {
	default_doc
	pkgdesc="Man pages for container tools"
}

sha512sums="
6420ca1b0b8c010db7481ce5219c67a0e38aa074f74b2d41c7995ed147a9acc38787d939b4a7c1193cb52f29dfb730c57a55a5f3d12d550032d05c1f4517adff  common-0.55.3.tar.gz
30c26b40f2858622ce4f63d21cee3ff626ac17f57ddcaf23a2d2d16b785259587cb7eda13309dc145860c3dd847b8546863eb0a32e63609586857c63e44b638a  storage-1.48.0.tar.gz
fa10e98acca566586add698ab9f269da0d2eff72480dd9dc027b0b39b66df7b7e0799ab76ce17e9a93f2117ef124226d48fd56cffb124d8a5f89b883f61b60cb  image-5.26.1.tar.gz
e875fd8f40be94a589ac2404e82e282490bdd639b75943f669feadf81a26abf9a1bf6649be7d9d2dd07e62df58af7d495c673f30c98cc27a2cf6e7892daf3f28  podman-4.6.1.tar.gz
8d321fba452784e3704c058113eb29c76a26e71aa19d0ba2c48fa077e0b74a64a1b8c4ba121d35a1d9ba886a5c28f2e66891e3f592654f2f738d8659443179b3  skopeo-1.13.2.tar.gz
c8611016448de7e9548ff692d1b352d7234f357440001106276e513df135a28ba0d4413a14f5c30e4b88615267d92f4aea63e1687b84a0638f1f9ab4f348c74a  buildah-1.31.2.tar.gz
856dbbeb2acda276e9605bd1ecec0f8d65952c597ee2af61dd8909d7d3c04e5ef06c40b69ec4a98f79e623c536850f614c1b0af3a19637e300e7d3a285933193  shortnames-2023.02.20.tar.gz
"
