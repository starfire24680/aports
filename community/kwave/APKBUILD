# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kwave
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kwave.sourceforge.net/"
pkgdesc="A sound editor for KDE"
license="GPL-2.0-or-later"
makedepends="
	alsa-lib-dev
	audiofile-dev
	extra-cmake-modules
	fftw-dev
	flac-dev
	karchive-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kservice-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libsamplerate-dev
	libvorbis-dev
	opus-dev
	pulseaudio-dev
	qt5-qtbase-dev
	qt5-qtmultimedia-dev
	rsvg-convert
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/multimedia/kwave.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kwave-$pkgver.tar.xz"
subpackages="$pkgname-libs $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DWITH_OSS=OFF \
		-DWITH_MP3=OFF
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
bac31f0a89ddea5d7dc7acacfd2494fe159034322fab8d5010a3dbc6f86e6dcd5bac627d7af002240d5ef4a93589f74fa3bdd6eda6c863db453feff9460dc865  kwave-23.08.0.tar.xz
"
