# Contributor: Orhun Parmaksız <orhunparmaksiz@gmail.com>
# Maintainer: Orhun Parmaksız <orhunparmaksiz@gmail.com>
pkgname=rustypaste
pkgver=0.13.0
pkgrel=0
pkgdesc="Minimal file upload/pastebin service"
url="https://github.com/orhun/rustypaste"
# s390x, ppc64le, riscv64: blocked by ring crate
# x86: test failure
arch="all !s390x !ppc64le !riscv64 !x86"
license="MIT"
makedepends="cargo zstd-dev openssl-dev cargo-auditable"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/orhun/rustypaste/archive/v$pkgver.tar.gz"
options="net"

prepare() {
	default_prepare

	# Rust target triple.
	local target=$(rustc -vV | sed -n 's/host: //p')

	# Build against system-provided libzstd.
	mkdir -p .cargo
	cat >> .cargo/config.toml <<-EOF
		[target.$target]
		zstd = { rustc-link-lib = ["zstd"] }
	EOF

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release --no-default-features --features openssl
}

check() {
	cargo test --frozen -- --test-threads 1
}

package() {
	install -Dm 755 "target/release/$pkgname" -t "$pkgdir/usr/bin"
	install -Dm 644 config.toml -t "$pkgdir/etc/rustypaste"
	install -Dm 644 README.md -t "$pkgdir/usr/share/doc/$pkgname"
	install -Dm 644 LICENSE -t "$pkgdir/usr/share/licenses/$pkgname"
}

sha512sums="
c20695c2dba3fe24a8ca5951b362932a52af8d36cf0dfa45174209d54698745e1a3c3d1a411e1d287b650006e7e1ab7e51a982bd3c58b9e53ede2127b2e1e197  rustypaste-0.13.0.tar.gz
"
