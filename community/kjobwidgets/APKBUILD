# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kjobwidgets
pkgver=5.109.0
pkgrel=0
pkgdesc="Widgets for tracking KJob instances"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends_dev="
	kcoreaddons-dev
	kwidgetsaddons-dev
	qt5-qtx11extras-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	qt5-qttools-dev
	samurai
	"
_repo_url="https://invent.kde.org/frameworks/kjobwidgets.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kjobwidgets-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/kjobwidgets.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
362bb6037766e3d059c4449e88637a28a8e8b4ecacd54084fb107c237676f94330cc5c21138797d7b9f94f693a6c757ecc9c82684f58903d005ea17f4bdce92c  kjobwidgets-5.109.0.tar.xz
"
