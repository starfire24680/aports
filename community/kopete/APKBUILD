# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kopete
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://userbase.kde.org/Kopete"
pkgdesc="An instant messenger supporting AIM, ICQ, Jabber, Gadu-Gadu, Novell GroupWise Messenger, and more"
license="GPL-2.0-or-later AND LGPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	gpgme-dev
	libidn-dev
	kcmutils-dev
	kconfig-dev
	kcontacts-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdelibs4support-dev
	kdnssd-dev
	kdoctools-dev
	kemoticons-dev
	khtml-dev
	ki18n-dev
	kidentitymanagement-dev
	kitemmodels-dev
	knotifyconfig-dev
	kparts-dev
	ktexteditor-dev
	kwallet-dev
	libkleo-dev
	qt5-qtbase-dev
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/network/kopete.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kopete-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
5e65a5dc64cb1cc028dfe2326ec2194dffe732e14935f9b8607444ab5cbc0b1b65ea292cbb6d8d0e002cee5de49d087fca0db8adbdb83a714c53002be2eff223  kopete-23.08.0.tar.xz
"
