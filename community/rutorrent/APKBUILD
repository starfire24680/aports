# Contributor: Francesco Colis_ta <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=rutorrent
_pkgname=ruTorrent
pkgver=4.2.5
pkgrel=0
pkgdesc="Yet another web front-end for rTorrent"
url="https://github.com/Novik/ruTorrent"
arch="noarch"
license="GPL-3.0-or-later"
depends="php81 curl"
options="!check" # no test available
# we need to user gnu tar since the permissions on files in tar archive are
# bad
makedepends="tar"
pkggroups="rutorrent"
install="$pkgname.pre-install $pkgname.post-install"
source="$pkgname-$pkgver.tar.gz::https://github.com/Novik/$_pkgname/archive/v$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	chmod -R u+w "$builddir"
}

package() {
	install -d $pkgdir/usr/share/webapps/$pkgname
	install -d $pkgdir/var/www/localhost/htdocs/
	cp -r ./* $pkgdir/usr/share/webapps/$pkgname
	rm -fr $pkgdir/usr/share/webapps/$pkgname/tests
	chgrp -R rutorrent $pkgdir/usr/share/webapps/$pkgname/share
	chmod 0775 $pkgdir/usr/share/webapps/$pkgname/share/settings
	chmod 0775 $pkgdir/usr/share/webapps/$pkgname/share/torrents
	chmod 0775 $pkgdir/usr/share/webapps/$pkgname/share/users
}

sha512sums="
4dd6b98b6f7221e1767af0d958fad2efe8b0dc6f6bc4500108f75401a474bf35c1ef11059a5ed45b67345a912db9d210f065a8e2452fadecae0c1d034dc96873  rutorrent-4.2.5.tar.gz
"
