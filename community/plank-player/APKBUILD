# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=plank-player
pkgver=5.27.7
pkgrel=1
pkgdesc="Multimedia Player for playing local files on Plasma Bigscreen"
url="https://invent.kde.org/plasma-bigscreen/plank-player"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
license="GPL-2.0-or-later"
depends="
	kirigami2
	qt5-qtgraphicaleffects
	qt5-qtmultimedia
	"
makedepends="
	extra-cmake-modules
	ki18n-dev
	kirigami2-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtmultimedia-dev
	qt5-qtquickcontrols2-dev
	samurai
	"
case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
_repo_url="https://invent.kde.org/plasma/plank-player.git"
source="https://download.kde.org/$_rel/plasma/$pkgver/plank-player-$pkgver.tar.xz"
options="!check" # No tests

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/plasma/plank-player.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
a129ad725a3293f928169aee261d422f5e0475c271063405dac9401ca580fc612f2f0b1a31c69d3f49a0e284d40128401ae0dc8b18b2434a839c6115395e053b  plank-player-5.27.7.tar.xz
"
