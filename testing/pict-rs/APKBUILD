# Contributor: Celeste <cielesti@protonmail.com>
# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=pict-rs
pkgver=0.4.2
pkgrel=0
pkgdesc="Simple image hosting service written in Rust"
url="https://git.asonix.dog/asonix/pict-rs"
# ppc64le, riscv64, s390x: ring crate
arch="all !ppc64le !riscv64 !s390x"
license="AGPL-3.0-or-later"
depends="exiftool ffmpeg imagemagick"
makedepends="cargo cargo-auditable"
install="$pkgname.pre-install"
pkgusers="pict-rs"
pkggroups="pict-rs"
subpackages="$pkgname-openrc"
source="pict-rs-$pkgver.tar.gz::https://git.asonix.dog/asonix/pict-rs/archive/v$pkgver.tar.gz
	config-file.patch
	pict-rs.initd
	"
builddir="$srcdir/pict-rs"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/pict-rs -t "$pkgdir"/usr/bin

	install -Dm640 -g pict-rs pict-rs.toml -t "$pkgdir"/etc/pict-rs
	install -Dm755 "$srcdir"/pict-rs.initd "$pkgdir"/etc/init.d/pict-rs
}

sha512sums="
45be55c44a7fef7f5baaed507c8977bc5a8e63fd6dd30081ae444201b29aad828898ba3e7bac2ff5b4af2ca1bb793a96063b76bca93cd1bcbb2b79a6831f3c39  pict-rs-0.4.2.tar.gz
3129cc14b76903c52328d854bcc5dd50ef73991db365ad2dd38a26ac49135addc64e52407c28835e5b5aa2adfd9ad4ee8b14d1c3f670608bdc4e6fb280328ec2  config-file.patch
6a9d40fc5c57d35a6601118eff551b4353f0e544045b953cc2728135bdea1e1f8945ffac889d805b988af8403e0114338cde31d8d977491a63270ce97b8cae73  pict-rs.initd
"
