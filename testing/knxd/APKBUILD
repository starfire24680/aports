# Maintainer: Cían Hughes <Ci@nHugh.es>
pkgname=knxd
pkgver=0.14.56
pkgrel=1
pkgdesc="Knxd is an advanced KNX router/gateway; it can talk to all known KNX interfaces"
arch="all"
url="https://github.com/knxd/knxd"
license="GPL-2.0-only"
makedepends="
	argp-standalone
	autoconf
	automake
	bsd-compat-headers
	fmt-dev
	libev-dev
	libtool
	libusb-dev
	linux-headers
	m4
	"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://codeload.github.com/knxd/knxd/tar.gz/$pkgver
	$pkgname-$pkgver-fmt10.patch::https://github.com/knxd/knxd/commit/797283251470786bffd6586571407337da899d30.patch"
options="!check" # no tests

prepare() {
	default_prepare

	printf "#!/bin/sh\n\necho %s" "$pkgver" > ./tools/version.sh
	sh ./bootstrap.sh
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--disable-systemd
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
37e9051209a7a5a8c42700548a4581aadc0bdf2966857f64b3802b0b74a85992ea65f15565344afda803acf34f87b73c98a156a9fcab4b6a3642dbac744df356  knxd-0.14.56.tar.gz
9aae4a071272f1bf654a38d98d29879180df04caabf25f5e3a493d38ae4cf357c2d0b2acd7e328ebc4eddaaaeb92441ea79d696c6c86455087efa34c932acb35  knxd-0.14.56-fmt10.patch
"
