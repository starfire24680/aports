# Contributor: Fabricio Silva <hi@fabricio.dev>
# Maintainer: Fabricio Silva <hi@fabricio.dev>
pkgname=py3-plexapi
_pkgname=python-plexapi
pkgver=4.15.1
pkgrel=0
pkgdesc="Python bindings for the Plex API"
url="https://github.com/pkkid/python-plexapi"
arch="noarch"
license="BSD-3-Clause"
# tests requires an instance of plex running
# net for sphinx
options="net !check"
depends="
	python3
	py3-requests
	"
makedepends="
	py3-gpep517
	py3-recommonmark
	py3-setuptools
	py3-sphinx_rtd_theme
	py3-wheel
	"
subpackages="$pkgname-doc $pkgname-pyc"
source="https://github.com/pkkid/python-plexapi/archive/$pkgver/py3-plexapi-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
	sphinx-build -W -b man docs man
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
	install -Dm644 man/pythonplexapi.1 -t "$pkgdir"/usr/share/man/man1
}

sha512sums="
e88b34356d67f339eaee1a40631fb0e4ff3252d2262228f32670d8226524fd67b16f321e3af7e3d7887dc2656dbc5538219faef16aa13d112b5cb41ef92afedd  py3-plexapi-4.15.1.tar.gz
"
