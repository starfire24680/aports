# Contributor: Breno Leitao <breno.leitao@gmail.com>
# Maintainer: Will Sinatra <wpsinatra@gmail.com>
pkgname=font-unifont
pkgver=15.0.06
pkgrel=1
pkgdesc="GNU Unifont glyphs"
url="https://unifoundry.com/unifont/index.html"
arch="all"
options="!check"  # No way to test fonts.
license="GFDL-1.3-or-later GPL-2.0-or-later WITH Font-exception-2.0"
subpackages="unifont-dev unifont-doc unifont-misc unifont-tools"
source="https://unifoundry.com/pub/unifont/unifont-$pkgver/unifont-$pkgver.tar.gz"
builddir="$srcdir/unifont-$pkgver"

prepare() {
	default_prepare
	# clean precompiled
	make clean
}

build() {
	make
}

package() {
	# renamed from unifont
	provides="unifont=$pkgver-r$pkgrel"
	replaces="unifont"

	# only keep OTF
	rm font/precompiled/*.ttf
	make PREFIX="$pkgdir/usr" install

	cd "$pkgdir"/usr/share/unifont
	rm -r html ChangeLog* INSTALL* NEWS* README* ./*.pdf
}

tools() {
	depends="bdftopcf perl-gd"
	
	amove usr/bin
}

doc() {
	default_doc
	amove usr/share/unifont/*.info.*
}

misc() {
	pkgdesc="$pkgdesc (misc hex/bmp files)"

	amove usr/share/unifont
}

sha512sums="
7c9b22099e06f988da01c7c96076bc2cf90ccc15d95a91c2ce7e7b4c168cf6876827e4b2e4235948474f8db54130b1495ca6166bb9f71692b49be1f40ef604cc  unifont-15.0.06.tar.gz
"
